package com.tbb.ump.domain;

// Generated 14 Aug, 2012 9:21:42 PM by Hibernate Tools 4.0.0

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * RecipeFoodItem generated by hbm2java
 */
@Entity
@Table(name = "TBB_UMP_RecipeFoodItem" )
public class RecipeFoodItem implements java.io.Serializable {

	private long id;
	private Long recipeId;
	private Long foodId;
	private Integer quantity;
	private Integer measure;

	public RecipeFoodItem() {
	}

	public RecipeFoodItem(long id) {
		this.id = id;
	}

	public RecipeFoodItem(long id, Long recipeId, Long foodId,
			Integer quantity, Integer measure) {
		this.id = id;
		this.recipeId = recipeId;
		this.foodId = foodId;
		this.quantity = quantity;
		this.measure = measure;
	}

	@Id
	@Column(name = "id_", unique = true, nullable = false)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "recipeId")
	public Long getRecipeId() {
		return this.recipeId;
	}

	public void setRecipeId(Long recipeId) {
		this.recipeId = recipeId;
	}

	@Column(name = "foodId")
	public Long getFoodId() {
		return this.foodId;
	}

	public void setFoodId(Long foodId) {
		this.foodId = foodId;
	}

	@Column(name = "quantity")
	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Column(name = "measure")
	public Integer getMeasure() {
		return this.measure;
	}

	public void setMeasure(Integer measure) {
		this.measure = measure;
	}

}
