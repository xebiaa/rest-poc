package com.tbb.ump.domain;

// Generated 14 Aug, 2012 9:21:42 PM by Hibernate Tools 4.0.0

import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * UserFavoriteFoodRecipe generated by hbm2java
 */
@Entity
@Table(name = "TBB_UMP_UserFavoriteFoodRecipe" )
public class UserFavoriteFoodRecipe implements java.io.Serializable {

	private UserFavoriteFoodRecipeId id;
	private String contentType;
	private String name;
	private long userId;
	private Date createdDate;

	public UserFavoriteFoodRecipe() {
	}

	public UserFavoriteFoodRecipe(UserFavoriteFoodRecipeId id, long userId) {
		this.id = id;
		this.userId = userId;
	}

	public UserFavoriteFoodRecipe(UserFavoriteFoodRecipeId id,
			String contentType, String name, long userId, Date createdDate) {
		this.id = id;
		this.contentType = contentType;
		this.name = name;
		this.userId = userId;
		this.createdDate = createdDate;
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "id", column = @Column(name = "id_", nullable = false)),
			@AttributeOverride(name = "contentId", column = @Column(name = "contentId", nullable = false)) })
	public UserFavoriteFoodRecipeId getId() {
		return this.id;
	}

	public void setId(UserFavoriteFoodRecipeId id) {
		this.id = id;
	}

	@Column(name = "contentType", length = 75)
	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	@Column(name = "name", length = 75)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "userId", nullable = false)
	public long getUserId() {
		return this.userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdDate", length = 19)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}
