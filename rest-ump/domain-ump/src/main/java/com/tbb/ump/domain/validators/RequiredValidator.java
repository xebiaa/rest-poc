/**
 * 
 */
package com.tbb.ump.domain.validators;

import java.util.Collection;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.stereotype.Component;

/**
 * @author Jagmeet
 *
 */
@Component
public class RequiredValidator implements ConstraintValidator<Required, Object> {

    @Override
    public void initialize(Required constraintAnnotation) {
	
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
	boolean isValid = Boolean.FALSE;
	
	if (value != null) {
	    if (value instanceof String
		    &&  ((String) value).trim().length() > 0) {
		isValid = Boolean.TRUE;
	    } else if (value instanceof Collection<?> 
		    && !((Collection<?>) value).isEmpty()) { 
		isValid = Boolean.TRUE;
	    } else if (!(value instanceof String) 
		    && !(value instanceof Collection<?>)){
		isValid = Boolean.TRUE;
	    }
	}
	return isValid;
    }
}