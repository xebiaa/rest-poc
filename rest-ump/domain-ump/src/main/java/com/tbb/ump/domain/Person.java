package com.tbb.ump.domain;

import com.tbb.ump.domain.validators.Required;

public class Person {
    
    @Required
    private String firstName;

    @Required
    private String lastName;

    public Person() {
    }

    public Person(String firstName, String lastName) {
	this.firstName = firstName;
	this.lastName = lastName;
    }

    public String getFirstName() {
	return this.firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public String getLastName() {
	return this.lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

}
