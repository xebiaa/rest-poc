/**
 * 
 */
package com.tbb.ump.common.constants;

/**
 * @author Jagmeet
 *
 */
public interface ICommonConstants {
	String UPLOADED_IMAGE_ID = "uploadedImageId";
	String UPLOADED_DISPLAY_IMAGE_ID = "uploadedDisplayImageId";
	String UPLOADED_DISPLAY_IMAGE_TOKEN = "uploadedDisplayImageToken";
	String MEAL_ORDER_NUM = "mealOrderNum";
	String ERROR = "error";
	String ERROR_CODE = "errorCode";
	String ERROR_TEXT = "errorText";
}
