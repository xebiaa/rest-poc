package com.tbb.ump.common;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public interface Constants {

    String SEARCH_COUNT = "searchCount";
    String SEARCH_LIST = "searchList";
    String EDIT_TYPE_BB = "BB";
    String SEARCH_ALL_RESULTS = "";
    String ACTION_PARAM = "myaction";
    int MINIMUM_CHARS_FOR_SEARCH = 3;
    String RECIPE_MODEL = "recipeModel";
    int MINS_IN_AN_HOUR = 60;
    String GLOBAL_ERROR_FIELD = "globalErrorField";
    String GLOBAL_SUCCESS_FIELD = "globalSuccessField";
    String SUCCESS_MESSAGE_PARAM = "successMessage";
    String ERROR_MESSAGE_PARAM = "errorMessage";
    int MEAL_PLAN_DAY_RANGE = 15;
    int MAX_MEAL_PLAN_DAYS = 120;
    String CALORIES = "Calories";
    String CARBS = "Carbs";
    String FATS = "Fats";
    String PROTEINS = "Proteins";
    String SHOW_TAGS = "showTags";
    String FOOD_TYPE_PARAM = "type";
    String SERVER_ERROR_CODE = "500";
    String NUTRIENT_FORMAT = "###0.00";
    String FOOD = "FOOD";
    String RECIPE = "RECIPE";

    String SEARCH_RESULT_ID_PARAM = "searchResultId";
    String FOOD_MODEL = "foodModel";
    String MEAL_PLAN_MODEL = "mealPlanModel";
    String EDIT_MEAL_PLAN_MODEL = "editMealPlanModel";
    String DATE_FORMAT_STRING = "MM/dd/yyyy";
    SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_STRING);
    String NOT_APPLICABLE = "N/A";
    String COPY_MEALPLAN_MODEL = "copyMealPlanModel";
    String COPY_SUCCESS = "successCopy";

    // Tab constants
    String CURRENT_TAB_KEY = "currentTab";
    String FOOD_TAB_VALUE = "food";
    String RECIPE_TAB_VALUE = "recipe";
    String MEAL_TAB_VALUE = "meal";
    String SUPER_ADMIN_TAB_VALUE = "superAdmin";

    // WaterTracker Constants
    String WATER_CONTAINER_ID = "waterContainerId";
    String CUPS_OF_WATER = "cupsOfWater";

    String DATE_PATTERN = "MM-dd-yyyy";
    String PST_TIME_ZONE_ID = "America/Los_Angeles";
    TimeZone PST_TIME_ZONE = TimeZone.getTimeZone(PST_TIME_ZONE_ID);

    long MILLISECONDS_IN_DAY = 1000 * 60 * 60 * 24;

    String CALENDAR_DATE_CHANGED_EVENT = "calendarDateChangedEvent";
    String CALENDAR_DATE = "calendardate";
    String MEAL_TRACKER_DATE_CHANGED_EVENT = "mealTrackerDateChangedEvent";

    int FIRST_DAY_ORDER_NO = 1;
    int ADDITIONAL_SNACK_ORDER_NO = 6;

    int ADDITIONAL_SNACK_MEAL_TYPE_ID = 6;

    long MEAL_TYPE_BREAKFAST_ID = 1;
    long MEAL_TYPE_SNACK1_ID = 2;
    long MEAL_TYPE_LUNCH_ID = 3;
    long MEAL_TYPE_SNACK2_ID = 4;
    long MEAL_TYPE_DINNER_ID = 5;
    long MEAL_TYPE_ALL = 0;
    long MEAL_TYPE_SUPPLEMENT_ID = 0;

    long MEAL_TAB_DEFAULT_ID = 1;
    long MEAL_TAB_SHAKEOLOGY_CHOCOLATE_ID = 2;
    long MEAL_TAB_SHAKEOLOGY_GREENBERRY_ID = 3;
    long MEAL_TAB_SHAKEOLOGY_TROPICAL_ID = 4;
    long MEAL_TAB_PROTEIN_BAR_ID = 5;

    long MEAL_TYPE_ADDITIONAL_SNACK_ID = ADDITIONAL_SNACK_MEAL_TYPE_ID;

    String MEAL_TRACKER_CURRENT_MENU_ITEM = "mealTrackerCurrentMenuItem";
    String USER_MEAL_DAY_VIEW_PAGE_RELATIVE_URL = "USER_MEAL_DAY_VIEW_PAGE_RELATIVE_URL";
    String RECIPE_CORNER_VIEW_PAGE_RELATIVE_URL = "RECIPE_CORNER_VIEW_PAGE_RELATIVE_URL";
    String ANALYTICS_VIEW_PAGE_RELATIVE_URL = "ANALYTICS_VIEW_PAGE_RELATIVE_URL";
    String INFORMATION_VIEW_PAGE_RELATIVE_URL = "INFORMATION_VIEW_PAGE_RELATIVE_URL";
    String CHANGE_MEAL_PLAN_RELATIVE_URL = "CHANGE_MEAL_PLAN_RELATIVE_URL";
    String PREFERENCES_RELATIVE_URL = "PREFERENCES_RELATIVE_URL";
    String USER_PROFILE_RELATIVE_URL = "USER_PROFILE_RELATIVE_URL";
    String SEND_MEAL_PLAN_RELATIVE_URL = "SEND_MEAL_PLAN_RELATIVE_URL";
    String SUPER_GYM_RELATIVE_URL = "SUPER_GYM_RELATIVE_URL";
    String MEAL_TYPE_SUPPLEMENT = "Supplement";
    String SOLR_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:sss'Z'";
    String SUCCESS_LIST_PAGE = "SUCCESS_LIST_PAGE";
    String SUCCESS_DETAIL_PAGE = "SUCCESS_DETAIL_PAGE";
    // Indexer fields
    String SOLR_CREATED_ON = "createdOn";
    String SOLR_RATING = "rating";
    String SOLR_STATUS = "status";
    String SOLR_XML_CONTENT = "xmlContent";
    String SOLR_TAGS = "tags";
    String SOLR_TOTAL_SCORES = "totalScores";
    String SOLR_NAME = "firstName";
    String SOLR_FOOD_NAME = "foodName";

    String SORT_BY_MOST_RECENT = "mostRecent";
    String SORT_BY_A_Z = "AZ";
    String SORT_BY_Z_A = "ZA";

    int DAYS_IN_WEEK = 7;
    int ACCEPTABLE_DISTANCE_IN_WEEK = 3;
    String SOLR_FIRST_NAME = "firstName";
    String MOST_EATEN = "MostEaten";

    String UMP_EMAIL_FROM_ADDRESS = "UMP_EMAIL_FROM_ADDRESS";

    int SUPPLEMENT_MEAL_ORDER = 0;

}
