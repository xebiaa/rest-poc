package com.tbb.ump.common.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;

import com.tbb.ump.common.Constants;


public class UMPDateUtil {

    private UMPDateUtil() {}

	private static final String DEFAULT_DATE_FORMAT_STRING = "MM-dd-yyyy HH:mm:ss";

    public static Date getDateInPST() throws ParseException {
        TimeZone timeZone = TimeZone.getTimeZone(Constants.PST_TIME_ZONE_ID);
        return changeTimeZoneOfUTCDate(new Date(), timeZone);
    }

    public static Date getDateInTimeZone(String timeZoneId) throws ParseException {
        TimeZone timeZone = TimeZone.getTimeZone(timeZoneId);
        return changeTimeZoneOfUTCDate(new Date(), timeZone);
    }

    public static Date convertTimeZone(Date date, String timeZoneFrom, String timeZoneTo) throws ParseException {
    	DateFormat fromDateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT_STRING);
    	fromDateFormat.setTimeZone(TimeZone.getTimeZone(timeZoneFrom));
    	
    	DateFormat toDateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT_STRING);
    	toDateFormat.setTimeZone(TimeZone.getTimeZone(timeZoneTo));
    	Date fromDate = fromDateFormat.parse(DateFormatUtils.format(date, DEFAULT_DATE_FORMAT_STRING));
		
    	Date toDate = DateUtils.parseDate(toDateFormat.format(fromDate), new String[]{DEFAULT_DATE_FORMAT_STRING});
    	
		return toDate;
    }
    
    public static Date convertDateToPSTFromUserTimeZone(Date date, String timeZoneId) throws ParseException {
        return zeroDownTimeFromDate(convertTimeZone(date, timeZoneId, Constants.PST_TIME_ZONE_ID));
    }
    
    
    public static Date convertUMPDateFormat(Date date) throws ParseException{
    	DateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT_STRING);
    	return zeroDownTimeFromDate(DateUtils.parseDate(dateFormat.format(date), new String[]{DEFAULT_DATE_FORMAT_STRING}));
    }


    private static Date changeTimeZoneOfUTCDate(Date utcDate, TimeZone zone) {
        Calendar first = Calendar.getInstance(zone);
        first.setTimeInMillis(utcDate.getTime());

        Calendar output = Calendar.getInstance();

        output.set(Calendar.DAY_OF_MONTH, first.get(Calendar.DAY_OF_MONTH));
        output.set(Calendar.MONTH, first.get(Calendar.MONTH));
        output.set(Calendar.YEAR, first.get(Calendar.YEAR));
        output.set(Calendar.HOUR_OF_DAY, first.get(Calendar.HOUR_OF_DAY));
        output.set(Calendar.MINUTE, first.get(Calendar.MINUTE));
        output.set(Calendar.SECOND, first.get(Calendar.SECOND));
        output.set(Calendar.MILLISECOND, first.get(Calendar.MILLISECOND));

        return output.getTime();
    }

    public static Date getFormattedDate(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_PATTERN);
        return sdf.parse(date);
    }

    public static Date zeroDownTimeFromDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        // Set time fields to zero
        zeroTime(cal);

        // Put it back in the Date object
        return cal.getTime();
    }

    public static Calendar getZeroDownCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        // Set time fields to zero
        zeroTime(cal);

        // Put it back in the Date object
        return cal;
    }


    private static void zeroTime(Calendar cal) {
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
    }

    /**
     * Returns the today's date in the specified TimeZone.
     * It zeros the hours, seconds, minutes, etc.
     *
     * @param timeZoneId
     * @return
     * @throws ParseException 
     */
    public static Calendar getToday(String timeZoneId) throws ParseException {
    	Date today = getDateInTimeZone(timeZoneId);
        today = zeroDownTimeFromDate(today);
        
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(today);
    	
        // Put it back in the Date object
        return cal;
    }

    /**
     * Returns the today's date in the specified TimeZone.
     * It zeros the hours, seconds, minutes, etc.
     *
     * @param timeZone
     * @return
     * @throws ParseException 
     */
    public static Calendar getToday(TimeZone timeZone) throws ParseException {
        return getToday(timeZone.getID());
    }

    //public Date getPSTDate

    public static Calendar getCalendarForDateString(String date, String pattern) throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(DateUtils.parseDate(date, new String[]{pattern}));

        return cal;
    }
    
    public static int getAge(String userTimeZoneId ,Date userDirthDate) throws ParseException {
    	int age;
    	Date currentDate = zeroDownTimeFromDate(getDateInTimeZone(userTimeZoneId));
    	Date birthDate = zeroDownTimeFromDate(userDirthDate);
    	
    	Calendar currentCalendar = Calendar.getInstance();
    	Calendar birthCalendar = Calendar.getInstance();
    	
    	currentCalendar.setTime(currentDate);
    	birthCalendar.setTime(birthDate);
    	
    	int negativeOffset = 0;
    	if (currentCalendar.get(Calendar.MONTH) < birthCalendar.get(Calendar.MONTH)
    			|| (currentCalendar.get(Calendar.MONTH) == birthCalendar.get(Calendar.MONTH)
    			&& currentCalendar.get(Calendar.DATE) < birthCalendar.get(Calendar.DATE))) {
    		negativeOffset -= 1;
    	}
    	age = currentCalendar.get(Calendar.YEAR) - birthCalendar.get(Calendar.YEAR) + negativeOffset;
    	
    	return age;
    }
    
    public static Date getUserCurrentDateInPST(String userTimeZoneId) throws ParseException {
		Date currentDateInUserTimeZone = zeroDownTimeFromDate(UMPDateUtil.getDateInTimeZone(userTimeZoneId));
		Date currentUserDateInPST = convertDateToPSTFromUserTimeZone(currentDateInUserTimeZone, userTimeZoneId);

		return currentUserDateInPST;
	}
    
    /**
     * The method returns the day of Week for a date.
     * @param date -String date passed
     * @param user -Logged in User, The locale will be picked from the user.
     * @return String: day of week.
     * @throws ParseException
     */
    public static String getDayOfWeekFromDate(String date,Locale locale) throws ParseException{
    	
    	Date formattedDate = getFormattedDate(date);
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(formattedDate);
    	if(locale == null) {
    		locale = new Locale("en_US");
    	}
    	String dayOfWeek = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, locale);
    	
    	return dayOfWeek;
    }
    
    /**
     * The method increment or decrements the passed date with the number of days passed.
     * @param date - the date to be altered.
     * @param days - the amount of increment or decrement.
     * @return - A new date object after incrementing or decrementing the passed date.
     */
    public static Date addDays(Date date, int days) {
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(date);
    	calendar.add(Calendar.DAY_OF_MONTH, days);
    	
    	return calendar.getTime();
    }

    /**
     * Formats a date in Constants.DATE_PATTERN format.
     * @param date
     */
    public static String format(Date date) {
        return DateFormatUtils.format(date, Constants.DATE_PATTERN);
    }

    /**
     * Parses a date from String in Constants.DATE_PATTERN
     *
     * @param date
     * @return
     * @throws ParseException
     */
    public static Date parse(String date) throws ParseException {
        return DateUtils.parseDate(date, new String[]{Constants.DATE_PATTERN});
    }
    
    private static int getPstDateOffset(String userTimeZoneId ) throws ParseException {
		Date currentDateInUserTimeZone = UMPDateUtil.zeroDownTimeFromDate(UMPDateUtil.getDateInTimeZone(userTimeZoneId));
		Date currentUserDateInPST = UMPDateUtil.convertDateToPSTFromUserTimeZone(currentDateInUserTimeZone, userTimeZoneId);

		int offset = 0;
		if (currentDateInUserTimeZone.after(currentUserDateInPST)) {
			offset = 1;
		} else if (currentDateInUserTimeZone.before(currentUserDateInPST)) {
			offset = -1;
		}

		return offset;
	}

    /**
     * Converts the zero down date in PST time zone to user's time zone.
     * @param user
     * @param dateInPst
     * @return
     * @throws ParseException
     */
	public static Date getUserDateFromPst(String userTimeZoneId, Date dateInPst) throws ParseException {
		int offset = getPstDateOffset(userTimeZoneId);

		String userDateInPSTString = format(dateInPst);
		String[] userDateInPSTSplit = userDateInPSTString.split("/");

		Calendar pstCalendar = Calendar.getInstance();
		pstCalendar.set(Integer.parseInt(userDateInPSTSplit[2]), 
				Integer.parseInt(userDateInPSTSplit[0]) - 1, 
				Integer.parseInt(userDateInPSTSplit[1]));
		pstCalendar.add(Calendar.DATE, offset);

		return pstCalendar.getTime();
	}
    
}

