package com.tbb.ump.common.utils;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;

import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;

public class AuthUtil {

    private AuthUtil() {}

	/**
	 * Checks whether a user has specified roles
	 */
	public static boolean hasAnyRole(User user, String roleString) {
		String[] rolesToCheck = roleString.split(",");

		return hasAnyRole(user, rolesToCheck);
	}

	/**
	 * Checks whether a user has specified roles
	 */
	public static boolean hasAnyRole(User user, String[] rolesToCheck) {
		Collection<String> roleNames = getRoleNames(user);

		for (String role : rolesToCheck) {
			if (roleNames.contains(role.trim())) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Checks whether a user has specified roles
	 */
	public static boolean hasAnyRole(User user, List<String> rolesToCheck) {
		Collection<String> roleNames = getRoleNames(user);

		for (String role : rolesToCheck) {
			if (roleNames.contains(role.trim())) {
				return true;
			}
		}
		
		return false;
	}

	private static Collection<String> getRoleNames(User user) {
		List<Role> roles = user.getRoles();
		Collection<String> roleNames = CollectionUtils.collect(roles, new Transformer() {
			public Object transform(Object arg0) {
				Role role = (Role) arg0;
				return role.getName();
			}
		});
		return roleNames;
	}

}
