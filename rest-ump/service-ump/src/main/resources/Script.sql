
CREATE TABLE TBB_UMP_UserSavedSearch (
	id_ BIGINT NOT NULL,
	name VARCHAR(300),
	url VARCHAR(1000) NOT NULL,
	userId BIGINT NOT NULL,
	lastModifiedDate DATETIME,
	type_ VARCHAR(64),
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_UserFavoriteMeal (
	id_ BIGINT NOT NULL,
	name VARCHAR(1000),
	userId BIGINT NOT NULL,
	createdDate DATETIME,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_FoodAdditionalMeasure (
	id_ BIGINT NOT NULL,
	foodId BIGINT,
	quantity FLOAT,
	measureUnitId BIGINT NOT NULL,
	ounceWeight FLOAT,
	deducible BIT DEFAULT 0,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_FavoriteMealPlanXref (
	id_ BIGINT NOT NULL,
	favoriteMealId BIGINT NOT NULL,
	mealPlanId BIGINT NOT NULL,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_UserRecentItem (
	id_ BIGINT NOT NULL,
	userId BIGINT,
	createdDate DATETIME,
	lastModifiedDate DATETIME,
	userMealContentId BIGINT,
	type_ VARCHAR(75),
	name VARCHAR(128),
	calories DOUBLE,
	carbs DOUBLE,
	fat FLOAT,
	protein FLOAT,
	imageId BIGINT,
	quantity DOUBLE,
	measureUnitId BIGINT,
	mealTypeGroupId INT,
	recentItemType VARCHAR(75) DEFAULT 'RecentlyAdded',
	count INTEGER UNSIGNED DEFAULT 0,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_MealTab (
	id_ BIGINT NOT NULL,
	name VARCHAR(75),
	orderNo INT,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_MeasureUnit (
	id_ BIGINT NOT NULL,
	name VARCHAR(200) NOT NULL,
	commonAbbreviation VARCHAR(200),
	active_ BIT,
	forNewFood BIT,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_UserMealDay (
	id_ BIGINT NOT NULL,
	orderNo INT,
	adminMealDayId BIGINT,
	mealPlanId BIGINT,
	status VARCHAR(75),
	deleted BIT,
	complete BIT,
	createdDate DATE,
	lastModifiedDate DATETIME,
	mealDayDate DATE,
	userId BIGINT,
	workoutDone BIT DEFAULT 1,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_FavoriteMeal (
	id_ BIGINT NOT NULL,
	name VARCHAR(75),
	mealTypeId BIGINT,
	imageId BIGINT,
	displayImageId BIGINT,
	createdBy BIGINT,
	createdDate DATETIME,
	lastModifiedBy BIGINT,
	lastModifiedDate DATETIME,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_WaterContainer (
	id_ BIGINT NOT NULL,
	userId BIGINT,
	date_ DATE,
	cupsOfWater INT,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_Meal (
	id_ BIGINT NOT NULL,
	mealTypeId BIGINT,
	mealTabId BIGINT,
	mealDayId BIGINT,
	primary_ BIT,
	safe BIT,
	orderNo INT,
	imageId BIGINT,
	dislayImageId BIGINT,
	createdBy BIGINT,
	createdDate DATETIME,
	lastModifiedBy BIGINT,
	lastModifiedDate DATETIME,
	deleted BIT,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_MealProgram (
	id_ BIGINT NOT NULL,
	name VARCHAR(75),
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_ProgramMealProgramMapping (
	id_ BIGINT NOT NULL,
	programId BIGINT,
	mealProgramId BIGINT,
	phaseMapped BIT,
	phase INT,
	createdBy BIGINT,
	createdDate DATETIME,
	lastModifiedBy BIGINT,
	lastModifiedDate DATETIME,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_AdditionalMeasureMapping (
	id_ BIGINT NOT NULL,
	fromMeasureUnitId BIGINT,
	toMeasureUnitId BIGINT,
	conversionFactor DOUBLE,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_UserMeal (
	id_ BIGINT NOT NULL,
	mealTypeId BIGINT,
	mealDayId BIGINT,
	orderNo INT,
	imageId BIGINT,
	dislayImageId BIGINT,
	userId BIGINT NOT NULL,
	createdDate DATETIME,
	lastModifiedBy BIGINT,
	lastModifiedDate DATETIME,
	deleted BIT,
	mealTypeName VARCHAR(75),
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_MealPlan (
	id_ BIGINT NOT NULL,
	mealProgramId BIGINT,
	days BIGINT,
	calories FLOAT,
	carbs FLOAT,
	fat FLOAT,
	protein FLOAT,
	status VARCHAR(75),
	live BIT,
	liveMealPlanId BIGINT,
	workingMealPlanId BIGINT,
	createdBy BIGINT,
	createdDate DATETIME,
	lastModifiedBy BIGINT,
	lastModifiedDate DATETIME,
	defaultMaintenanceLoopSet BIT,
	maintenanceLoopStart INT,
	maintenanceLoopEnd INT,
	version INT,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_UserMealPlan (
	id_ BIGINT NOT NULL,
	adminMealPlanId BIGINT,
	days BIGINT,
	calories DOUBLE,
	carbs DOUBLE,
	fat FLOAT,
	status VARCHAR(75),
	protein DOUBLE,
	startDate DATE,
	createdDate DATE,
	lastModifiedDate DATETIME,
	userId BIGINT,
	name VARCHAR(128),
	mealPlanStartWeight DOUBLE,
	mealPlanGoalWeight DOUBLE,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_Recipe (
	id_ BIGINT NOT NULL,
	name VARCHAR(75),
	description VARCHAR(301),
	source VARCHAR(75),
	link VARCHAR(255),
	preparationTime INT,
	totalTime INT,
	yield FLOAT,
	servingSize FLOAT,
	servingMeasureUnitId BIGINT,
	imageId BIGINT,
	displayImageId BIGINT,
	parentId BIGINT,
	status VARCHAR(75),
	calories FLOAT,
	protein FLOAT,
	carbs FLOAT,
	fat FLOAT,
	iron FLOAT,
	calcium FLOAT,
	sodium FLOAT,
	fiber FLOAT,
	sugar FLOAT,
	cholesterol FLOAT,
	deleted BIT,
	createdBy BIGINT,
	createdDate DATETIME,
	lastModifiedBy BIGINT,
	lastModifiedDate DATETIME,
	approvedBy BIGINT,
	approvedDate DATETIME,
	submittedForApprovalBy BIGINT,
	submittedForApprovalDate DATETIME,
	saturatedFat FLOAT,
	vitaminA FLOAT,
	vitaminC FLOAT,
	potassium FLOAT,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_MealContent (
	id_ BIGINT NOT NULL,
	parentMealId BIGINT,
	parentMealType VARCHAR(75),
	contentId BIGINT,
	type_ VARCHAR(75),
	orderNo INT,
	quantity FLOAT,
	measureUnitId BIGINT,
	createdBy BIGINT,
	createdDate DATETIME,
	lastModifiedBy BIGINT,
	lastModifiedDate DATETIME,
	deleted BIT,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_MealDay (
	id_ BIGINT NOT NULL,
	orderNo INT,
	newOrderNo INT,
	mealPlanId BIGINT,
	parentId BIGINT,
	status VARCHAR(75),
	deleted BIT,
	live BIT,
	liveMealDayId BIGINT,
	createdBy BIGINT,
	createdDate DATETIME,
	lastModifiedBy BIGINT,
	lastModifiedDate DATETIME,
	approvedBy BIGINT,
	approvedDate DATETIME,
	submittedForApprovalBy BIGINT,
	submittedForApprovalDate DATETIME,
	version BIGINT,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_Program_MET_Values (
	id_ BIGINT,
	programId BIGINT,
	valueType VARCHAR(30),
	value FLOAT
) ENGINE=MyISAM;

CREATE TABLE TBB_UMP_RecipeFoodItem (
	id_ BIGINT NOT NULL,
	recipeId BIGINT,
	foodId BIGINT,
	quantity INT,
	measure INT,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_UserMealContent (
	id_ BIGINT NOT NULL,
	parentMealId BIGINT,
	parentMealType VARCHAR(75),
	contentId BIGINT,
	type_ VARCHAR(75),
	orderNo INT,
	quantity DOUBLE,
	measureUnitId BIGINT,
	userId BIGINT NOT NULL,
	createdDate DATETIME,
	lastModifiedBy BIGINT,
	lastModifiedDate DATETIME,
	deleted BIT,
	calories DOUBLE,
	carbs DOUBLE,
	fat DOUBLE,
	protein DOUBLE,
	consumed BIT,
	addedByuser BIT,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_RecipeFood (
	id_ BIGINT NOT NULL,
	recipeId BIGINT,
	foodId BIGINT,
	quantity FLOAT,
	orderNo INT,
	measureUnitId BIGINT,
	htmlDescription VARCHAR(1000),
	includeInGroceryList BIT,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_UserOnboardingPreferences (
	id_ BIGINT NOT NULL,
	userId BIGINT,
	createdDate DATETIME,
	lastModifiedDate DATETIME,
	includeShakeology VARCHAR(70),
	shakeologyFor VARCHAR(75),
	shakeologyFlavor VARCHAR(75),
	includeProteinBar TINYINT,
	goal VARCHAR(128),
	programId INT,
	phase INT,
	BMR DECIMAL(10 , 0),
	activityLevel VARCHAR(75),
	mealPlanPendingActivation BIT,
	mealPlanId BIGINT,
	metIntensityLevel VARCHAR(10),
	workoutStartDate DATE,
	workoutTimePreference VARCHAR(20),
	heightCm INT,
	measureScale VARCHAR(10),
	maxStepCompleted INT,
	facebookPhotoUploaded BIT,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_RecipePrepSteps (
	id_ BIGINT NOT NULL,
	description VARCHAR(500),
	orderNo INT,
	recipeId BIGINT,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_UserFavoriteFoodRecipe (
	id_ BIGINT NOT NULL,
	contentId BIGINT NOT NULL,
	contentType VARCHAR(75),
	name VARCHAR(75),
	userId BIGINT NOT NULL,
	createdDate DATETIME,
	PRIMARY KEY (id_,contentId)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_SavedSearch (
	id_ BIGINT NOT NULL,
	name VARCHAR(300),
	url VARCHAR(1000) NOT NULL,
	userId MEDIUMTEXT NOT NULL,
	lastModifiedOn DATETIME,
	type_ VARCHAR(64),
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_Food (
	id_ BIGINT NOT NULL,
	NDB_No BIGINT,
	name VARCHAR(500) NOT NULL,
	quantity FLOAT,
	measureUnitId BIGINT,
	ounceWeight FLOAT,
	calories FLOAT,
	protein FLOAT,
	carbs FLOAT,
	fiber FLOAT,
	sugar FLOAT,
	fat FLOAT,
	cholesterol FLOAT,
	calcium FLOAT,
	iron FLOAT,
	sodium FLOAT,
	link VARCHAR(255),
	description VARCHAR(512),
	imageId MEDIUMTEXT,
	status VARCHAR(75),
	deleted BIT,
	createdBy BIGINT,
	createdDate DATETIME,
	lastModifiedBy BIGINT,
	lastModifiedDate DATETIME,
	approvedBy BIGINT,
	approvedDate DATETIME,
	displayImageId BIGINT,
	submittedForApprovalBy BIGINT,
	submittedForApprovalDate DATETIME,
	type_ VARCHAR(75),
	parentId BIGINT,
	customFood BIT,
	saturatedFat FLOAT,
	vitaminA FLOAT,
	vitaminC FLOAT,
	potassium FLOAT,
	supplement BIT DEFAULT 0,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE TABLE TBB_UMP_MealType (
	id_ BIGINT NOT NULL,
	name VARCHAR(75),
	orderNo INT,
	PRIMARY KEY (id_)
) ENGINE=InnoDB;

CREATE INDEX fromMeasureUnitId ON TBB_UMP_AdditionalMeasureMapping (fromMeasureUnitId ASC);

CREATE INDEX mealPlanId ON TBB_UMP_FavoriteMealPlanXref (mealPlanId ASC);

CREATE INDEX adminMealDayId ON TBB_UMP_UserMealDay (adminMealDayId ASC);

CREATE INDEX measureUnitId ON TBB_UMP_FoodAdditionalMeasure (measureUnitId ASC);

CREATE INDEX mealDayId ON TBB_UMP_Meal (mealDayId ASC);

CREATE INDEX favoriteMealId ON TBB_UMP_FavoriteMealPlanXref (favoriteMealId ASC);

CREATE INDEX parentId ON TBB_UMP_Recipe (parentId ASC);

CREATE INDEX measureUnitId ON TBB_UMP_MealContent (measureUnitId ASC);

CREATE INDEX parentId ON TBB_UMP_MealDay (parentId ASC);

CREATE INDEX test1 ON TBB_UMP_MealDay (mealPlanId ASC);

CREATE INDEX liveMealDayId ON TBB_UMP_MealDay (liveMealDayId ASC);

CREATE INDEX recipeId ON TBB_UMP_RecipeFood (recipeId ASC);

CREATE INDEX recipeId ON TBB_UMP_RecipePrepSteps (recipeId ASC);

CREATE INDEX measureUnitId ON TBB_UMP_Food (measureUnitId ASC);

CREATE INDEX mealTabId ON TBB_UMP_Meal (mealTabId ASC);

CREATE INDEX foodId ON TBB_UMP_RecipeFood (foodId ASC);

CREATE INDEX mealTypeId ON TBB_UMP_FavoriteMeal (mealTypeId ASC);

CREATE INDEX measureUnitId ON TBB_UMP_RecipeFood (measureUnitId ASC);

CREATE INDEX adminMealPlanId ON TBB_UMP_UserMealPlan (adminMealPlanId ASC);

CREATE INDEX mealProgramId ON TBB_UMP_MealPlan (mealProgramId ASC);

CREATE INDEX mealPlanId ON TBB_UMP_UserMealDay (mealPlanId ASC);

CREATE INDEX mealTypeId ON TBB_UMP_UserMeal (mealTypeId ASC);

CREATE INDEX toMeasureUnitId ON TBB_UMP_AdditionalMeasureMapping (toMeasureUnitId ASC);

CREATE INDEX servingMeasureUnitId ON TBB_UMP_Recipe (servingMeasureUnitId ASC);

CREATE INDEX workingMealPlanId ON TBB_UMP_MealPlan (workingMealPlanId ASC);

CREATE INDEX mealDayId ON TBB_UMP_UserMeal (mealDayId ASC);

CREATE INDEX ump_food_ndb_no ON TBB_UMP_Food (NDB_No ASC);

CREATE INDEX mealTypeId ON TBB_UMP_Meal (mealTypeId ASC);

CREATE INDEX liveMealPlanId ON TBB_UMP_MealPlan (liveMealPlanId ASC);

CREATE INDEX TBB_UMP_MeasureUnit_Name ON TBB_UMP_MeasureUnit (name ASC);

CREATE INDEX id_ ON TBB_UMP_MeasureUnit (id_ ASC);

CREATE INDEX foodId ON TBB_UMP_FoodAdditionalMeasure (foodId ASC);

CREATE INDEX mealProgramId ON TBB_UMP_ProgramMealProgramMapping (mealProgramId ASC);

CREATE INDEX measureUnitId ON TBB_UMP_UserMealContent (measureUnitId ASC);

CREATE INDEX parentId ON TBB_UMP_Food (parentId ASC);

CREATE INDEX parentMealId ON TBB_UMP_MealContent (parentMealId ASC);

