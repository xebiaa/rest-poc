package com.tbb.ump.dao.impl;

import com.tbb.ump.dao.WaterContainerPersistence;
import com.tbb.ump.domain.WaterContainer;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.Query;
import java.util.Date;
import java.util.List;

@Repository
public class WaterContainerPersistenceImpl extends GenericDaoImpl<WaterContainer> implements
		WaterContainerPersistence {

	@Override
	public WaterContainer fetchByDateAndUserId(Date dateInPST, Long userId) {
		Query query = em.createQuery("select it from " + getType().getName() + " it  where  it.date = :dateInPST and it.userId =:userId");
		query.setParameter("dateInPST", dateInPST);
		query.setParameter("userId", userId);
		@SuppressWarnings("unchecked")
		List<WaterContainer> list = query.getResultList();
		if(!CollectionUtils.isEmpty(list)) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public List<WaterContainer> findByUserIdAndDateLessThan(long userId,
			Date date, int i, int numOfDays, String orderBy) {
		Query query = em.createQuery("select it from " + getType().getName() + " it where  it.date <= :dateInPST and it.userId =:userId Order by it.date desc");
		query.setParameter("dateInPST", date);
		query.setParameter("userId", userId);
		query.setFirstResult(i);
		query.setMaxResults(numOfDays);
		@SuppressWarnings("unchecked")
		List<WaterContainer> list = query.getResultList();
		return list;
	}
}
