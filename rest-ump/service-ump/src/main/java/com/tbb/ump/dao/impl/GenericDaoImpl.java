package com.tbb.ump.dao.impl;

import com.tbb.ump.dao.GenericDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;


public abstract class GenericDaoImpl<T> implements GenericDao<T> {

    @PersistenceContext
    protected EntityManager em;

    public EntityManager getEntityManager() {
		return em;
	}

	private Class<T> type;

    public Class<T> getType() {
		return type;
	}

	@SuppressWarnings("unchecked")
	public GenericDaoImpl() {
    	ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		this.type = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
    }
    
    public void setType(Class<T> type) {
    	this.type = type;
    }

    @Override
    public long countAll(final Map<String, Object> params) {

        final StringBuffer queryString = new StringBuffer(
                "SELECT count(o) from ");
        queryString.append(type.getSimpleName()).append(" o ");
        final Query query = this.em.createQuery(queryString.toString());

        return (Long) query.getSingleResult();

    }

    @Override
    public T create(final T t) {
        this.em.persist(t);
        return t;
    }

    @Override
    public void delete(final Object id) {
        this.em.remove(this.em.getReference(type, id));
    }

    @Override
    public T find(final Object id) {
        return (T) this.em.find(type, id);
    }

    @Override
    public T update(final T t) {
        return this.em.merge(t);    
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public List<T> findAll() {
    	return em.createQuery("select _it_ from " + type.getName() + " _it_").getResultList();
    }
    
    
}
