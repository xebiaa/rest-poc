package com.tbb.ump.dao;

import java.util.List;
import java.util.Map;

public interface GenericDao<T> {
	
	long countAll(Map<String, Object> params);

	List<T> findAll();

    T create(T t);

    void delete(Object id);

    T find(Object id);

    T update(T t);
}
