package com.tbb.ump.service.impl;

import com.tbb.ump.common.utils.UMPDateUtil;
import com.tbb.ump.dao.WaterContainerPersistence;
import com.tbb.ump.domain.WaterContainer;
import com.tbb.ump.service.WaterContainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.*;

@Service("waterContainerService")
public class WaterContainerServiceImpl implements WaterContainerService {

    @Autowired
    WaterContainerPersistence waterContainerPersistence;

    public WaterContainer addWaterContainer(Date userDate, Long userId,
                                            String userTimeZoneId) throws ParseException {

        Date dateInPST = UMPDateUtil.convertDateToPSTFromUserTimeZone(userDate,
                userTimeZoneId);

        if (waterContainerPersistence.fetchByDateAndUserId(dateInPST, userId) != null) {
            // throw new
            // DuplicateWaterContainerException("Water Container already exists.");
        }
        WaterContainer persistedWaterContainer = new WaterContainer();
        persistedWaterContainer.setCupsOfWater(0);
        persistedWaterContainer.setUserId(userId);
        persistedWaterContainer.setDate(dateInPST);
        WaterContainer addedWaterContainer = waterContainerPersistence
                .create(persistedWaterContainer);
        return addedWaterContainer;

    }

    /**
     * Returns the WaterContainer based on user and date. User will be in other
     * time-zone and we are storing date in PST in server. Hence we would need
     * to change the date into PST.
     */
    public WaterContainer getWaterContainer(Date userDate,
                                            String userTimeZoneId, Long userId) throws ParseException {
    	 System.out.println("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ11111" +userDate);
        Date dateInPST = UMPDateUtil.convertDateToPSTFromUserTimeZone(userDate,
                userTimeZoneId);
        System.out.println("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ" +dateInPST);
        WaterContainer persistedWaterContainer = waterContainerPersistence
                .fetchByDateAndUserId(dateInPST, userId);
        return persistedWaterContainer;
    }

    /**
     * Method to save number of water cups
     */
    public WaterContainer saveWaterCups(long waterContainerId,
                                        Integer numberOfCups) {
        WaterContainer waterContainer = waterContainerPersistence
                .find(waterContainerId);
        waterContainer.setCupsOfWater(numberOfCups);
        WaterContainer updatedWaterContainer = waterContainerPersistence
                .update(waterContainer);
        return updatedWaterContainer;

    }

    public WaterContainer addWaterCup(long waterContainerId,
                                      Integer numberOfCups) {
        WaterContainer waterContainer = waterContainerPersistence
                .find(waterContainerId);
        Integer originalNumberOfCups = waterContainer.getCupsOfWater();
        waterContainer.setCupsOfWater(originalNumberOfCups + numberOfCups);
        WaterContainer updatedWaterContainer = waterContainerPersistence
                .update(waterContainer);
        return updatedWaterContainer;
    }

    public WaterContainer subtractWaterCup(long waterContainerId,
                                           Integer numberOfCups) {
        WaterContainer waterContainer = waterContainerPersistence
                .find(waterContainerId);
        Integer originalNumberOfCups = waterContainer.getCupsOfWater();
        if (originalNumberOfCups >= numberOfCups) {
            waterContainer.setCupsOfWater(originalNumberOfCups - numberOfCups);
        } else {
            // TODO
            // throw new
            // CanNotRemoveWaterCupException("Trying to remove more water cups than actualy present");
        }

        WaterContainer updatedWaterContainer = waterContainerPersistence
                .update(waterContainer);
        return updatedWaterContainer;
    }

    /**
     * This method returns water containers for last n days from the day passed.
     */

    public List<WaterContainer> getWaterContainerForLastNDays(long userId,
                                                              Date date, int numOfDays) {
        Date startDate = UMPDateUtil.addDays(date, numOfDays * -1 + 1);
        List<WaterContainer> waterContainers = waterContainerPersistence
                .findByUserIdAndDateLessThan(userId, date, 0, numOfDays, "test");
        waterContainers = new LinkedList<WaterContainer>(waterContainers);
        Collections.reverse(waterContainers);
        Iterator<WaterContainer> waterContainerIterator = waterContainers
                .iterator();
        while (waterContainerIterator.hasNext()) {
            WaterContainer waterContainer = waterContainerIterator.next();
            if (waterContainer.getDate().before(startDate)) {
                waterContainerIterator.remove();
            } else {
                break;
            }
        }

        return waterContainers;
    }

}
