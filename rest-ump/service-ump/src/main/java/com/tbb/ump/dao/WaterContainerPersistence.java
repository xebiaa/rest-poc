package com.tbb.ump.dao;

import com.tbb.ump.domain.WaterContainer;

import java.util.Date;
import java.util.List;


public interface WaterContainerPersistence extends GenericDao<WaterContainer> {

	WaterContainer fetchByDateAndUserId(Date dateInPST, Long userId);

	List<WaterContainer> findByUserIdAndDateLessThan(long userId, Date date,
                                                     int i, int numOfDays, String orderBy);

}
