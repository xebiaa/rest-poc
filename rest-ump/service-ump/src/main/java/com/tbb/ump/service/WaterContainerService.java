package com.tbb.ump.service;

import com.tbb.ump.domain.WaterContainer;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.Date;
import java.util.List;


public interface WaterContainerService {

    @Transactional
    WaterContainer addWaterContainer(Date userDate, Long userId, String userTimeZoneId) throws ParseException;

    @Transactional(readOnly = true)
    WaterContainer getWaterContainer(Date userDate, String userTimeZoneId, Long userId) throws ParseException;

    @Transactional
    WaterContainer saveWaterCups(long waterContainerId, Integer numberOfCups);

    @Transactional
    WaterContainer addWaterCup(long waterContainerId, Integer numberOfCups);

    @Transactional
    WaterContainer subtractWaterCup(long waterContainerId, Integer numberOfCups);

    @Transactional(readOnly = true)
    List<WaterContainer> getWaterContainerForLastNDays(long userId, Date date, int numOfDays);

}
