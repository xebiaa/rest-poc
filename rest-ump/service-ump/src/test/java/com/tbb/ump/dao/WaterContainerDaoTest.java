package com.tbb.ump.dao;

import java.util.Date;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tbb.ump.domain.WaterContainer;


@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration({"/test-applicationContext-dao.xml"})
public class WaterContainerDaoTest {

    @Autowired
    private WaterContainerPersistence dao;


    @SuppressWarnings("deprecation")
    @org.junit.Test
    public void testFetchByDateAndUserId() {
        Date date = new Date();
        Long userId = 1L;
        for (int i = 1; i < 5; i++) {
            date.setDate(date.getDate() - 1);
            WaterContainer waterContainer = new WaterContainer(i, userId, date, i);
            dao.create(waterContainer);
        }
        Date noDataDate = new Date();
        noDataDate.setDate(noDataDate.getDate() - 10);
        Assert.assertEquals(1L, dao.fetchByDateAndUserId(date, userId).getId());
        Assert.assertEquals(4, dao.findByUserIdAndDateLessThan(userId, date, 0, 10, null).size());
        Assert.assertNull(dao.fetchByDateAndUserId(noDataDate, userId));
        Assert.assertEquals(0, dao.findByUserIdAndDateLessThan(userId, noDataDate, 0, 10, null).size());

    }


}