package com.tbb.ump.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tbb.ump.dao.WaterContainerPersistence;
import com.tbb.ump.domain.WaterContainer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/test-applicationContext-dao.xml", "/test-applicationContext-mock-daos.xml", "/META-INF/applicationContext-service.xml"})
public class WaterContainerServiceTest {

	@Resource
	WaterContainerPersistence waterContainerPersistence;

    @Resource
	WaterContainerService service;

	@Before
	public void init() {
		//service = new WaterContainerServiceImpl();
	}

	@Test
	public void testAddWaterContainer() throws ParseException {
		Long userId = 1L;
		Date userDate = new Date();
		String userTimeZoneId = "US";
		WaterContainer waterContainer = waterContainer(userId, userDate);
        when(waterContainerPersistence.fetchByDateAndUserId(any(Date.class), anyLong())).thenReturn(
				waterContainer);
		when(waterContainerPersistence.create(any(WaterContainer.class))).thenReturn(waterContainer);
		WaterContainer container = service.addWaterContainer(userDate, userId,
				userTimeZoneId);
		Assert.assertEquals(waterContainer, container);
	}

	@Test
	public void testGetWaterContainer() throws Exception {
		Long userId = 1L;
		Date userDate = new Date();
		String userTimeZoneId = "US";
		WaterContainer waterContainer = waterContainer(userId, userDate);
		when(waterContainerPersistence.fetchByDateAndUserId(any(Date.class), anyLong())).thenReturn(
				waterContainer);
		WaterContainer container = service.getWaterContainer(userDate,
				userTimeZoneId, userId);
		Assert.assertEquals(waterContainer, container);

	}

	@Test
	public void testSaveWaterCups() {
		Long waterContainerId = 1L;
		Integer numberOfCups = 5;
		Date userDate = new Date();
		WaterContainer waterContainer = waterContainer(1L, userDate);

		when(waterContainerPersistence.find(waterContainerId)).thenReturn(waterContainer);
		when(waterContainerPersistence.update(waterContainer)).thenReturn(waterContainer);
		WaterContainer container = service.saveWaterCups(waterContainerId,
				numberOfCups);
		Assert.assertEquals(waterContainer, container);
	}

	@Test
	public void testAddWaterCup() {
		Long waterContainerId = 1L;
		Integer numberOfCups = 5;
		Date userDate = new Date();
		WaterContainer waterContainer = waterContainer(1L, userDate);

		when(waterContainerPersistence.find(waterContainerId)).thenReturn(waterContainer);
		when(waterContainerPersistence.update(waterContainer)).thenReturn(waterContainer);
		WaterContainer container = service.addWaterCup(waterContainerId,
				numberOfCups);
		Assert.assertEquals(waterContainer, container);
	}

	@Test
	public void testSubtractWaterCup() {
		Long waterContainerId = 1L;
		Integer numberOfCups = 5;
		Date userDate = new Date();
		WaterContainer waterContainer = waterContainer(1L, userDate);

		when(waterContainerPersistence.find(waterContainerId)).thenReturn(waterContainer);
		when(waterContainerPersistence.update(waterContainer)).thenReturn(waterContainer);
		WaterContainer container = service.subtractWaterCup(waterContainerId,
				numberOfCups);
		Assert.assertEquals(waterContainer, container);
	}

	@Test
	public void testGetWaterContainerForLastNDays() {
		Long userId = 1L;
		int numOfDays = 5;
		Date userDate = new Date();
		WaterContainer waterContainer = waterContainer(1L, userDate);

		List<WaterContainer> values = new ArrayList<WaterContainer>();
		values.add(waterContainer);
		when(
				waterContainerPersistence.findByUserIdAndDateLessThan(userId, userDate, 0, numOfDays,
						"test")).thenReturn(values);
		List<WaterContainer> returnedList = service
				.getWaterContainerForLastNDays(userId, userDate, numOfDays);
		Assert.assertEquals(values, returnedList);
	}

	private WaterContainer waterContainer(Long userId, Date userDate) {
		WaterContainer waterContainer = new WaterContainer();
		waterContainer.setCupsOfWater(2);
		waterContainer.setDate(userDate);
		waterContainer.setUserId(userId);
		waterContainer.setId(1);
		return waterContainer;
	}

}