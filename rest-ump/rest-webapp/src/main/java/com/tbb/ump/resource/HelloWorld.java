package com.tbb.ump.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.sun.jersey.api.client.ClientResponse;
import com.tbb.ump.domain.Person;
import com.tbb.ump.resource.aspect.Validate;
import com.tbb.ump.resource.exception.ResourceBaseException;

/**
 * Created by IntelliJ IDEA.
 * User: vikas
 * Date: 8/10/12
 * Time: 4:10 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
@Path("sayhello")
public class HelloWorld {

    /**
     * Returns hello message.
     * @return
     */
    @GET
    @Produces({MediaType.TEXT_PLAIN})
    public String sayHello() {
	return "Hi Vikas";
    }

    /**
     * Returns person details.
     * @return
     */
    @GET
    @Path("person")
    @Produces(MediaType.APPLICATION_JSON)
    public Person getPersonDetails() {
	return new Person("Vikas", "Gupta");
    }

    @Validate
    @POST
    @Path("person/add")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Person savePerson(Person person) throws ResourceBaseException {
	return person;
    }

    /**
     * Returns person details.
     * @return
     * @throws com.tbb.ump.resource.exception.ResourceBaseException
     */
    @GET
    @Path("exceptionPath")
    @Produces(MediaType.APPLICATION_JSON)
    public Person exceptionPath() throws ResourceBaseException {
	throw new ResourceBaseException(ClientResponse.Status.BAD_REQUEST, "exceptionOccurred");
    }
}
