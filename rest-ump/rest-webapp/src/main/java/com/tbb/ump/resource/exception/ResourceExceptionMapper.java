/**
 * 
 */
package com.tbb.ump.resource.exception;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.stereotype.Component;
import org.springframework.validation.ObjectError;

/**
 * @author Jagmeet
 *
 */
@Component
@Provider
public class ResourceExceptionMapper implements ExceptionMapper<ResourceBaseException>, MessageSourceAware {
private static Logger _LOGGER = LoggerFactory.getLogger(ResourceExceptionMapper.class);
    
    private MessageSource messageSource;
    
    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public Response toResponse(ResourceBaseException e) {
        return Response.status(e.getStatus())  
            .type(MediaType.APPLICATION_JSON)  
            .entity(getMessages(e))  
            .build(); 
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    
    public String getAppSpecificMessage(ResourceBaseException e) {
        return messageSource.getMessage(e.getMessageKey(), new String[]{}, Locale.US);
    }
    
    public String getMessages(ResourceBaseException e) {
        
        if(e.hasValidationErrors()) {
            List<String> errorMessages = new ArrayList<String>(e.getErrors().getAllErrors().size());
            for(ObjectError error:e.getErrors().getAllErrors()) {
        	Object[] errorArguments = error.getArguments() == null ? new String[]{} : error.getArguments();
               errorMessages.add(messageSource.getMessage(error.getCode(), errorArguments, Locale.US));
            }
            String msg = "";
            try { 
                msg = objectMapper.writeValueAsString(errorMessages);
            } catch(IOException io) {
                _LOGGER.error("Could not serialize the errors list to JSON", io);
            }
            return msg;
        } else {
            return messageSource.getMessage(e.getMessageKey(), e.getMessageArgs() == null?new String[]{}:e.getMessageArgs(), Locale.US);
        }
    
    }
}
