package com.tbb.ump.resource.exception;

import org.springframework.validation.Errors;

import com.sun.jersey.api.client.ClientResponse.Status;

/**
 * @author Jagmeet
 *
 */
public class ResourceBaseException extends Exception {
    private static final long serialVersionUID = 1L;

    private Status status;
    private String messageKey;
    private Errors errors;
    private String[] messageArgs;
    
    public ResourceBaseException() {
        super();
    }
    
    public ResourceBaseException(String message) {
        super(message);
    }
    
    public ResourceBaseException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public ResourceBaseException(Status status, String messageKey) {
        this.status = status;
        this.messageKey = messageKey;
    }
    
    public ResourceBaseException(Status status, String messageKey, String[] messageArgs) {
        this(status, messageKey);
        this.messageArgs = messageArgs;
    }
    
    public ResourceBaseException(String message, Throwable cause, Status status) {
        super(message, cause);
        this.status = status;
    }
    
    public ResourceBaseException(String message, Throwable cause, Status status, String messageKey) {
        this(message, cause, status);
        this.messageKey = messageKey;
    }
    
    public ResourceBaseException(Status status, Errors errors) {
        this.status = status;
        this.errors = errors;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public Errors getErrors() {
        return errors;
    }

    public void setErrors(Errors errors) {
        this.errors = errors;
    }
    
    public boolean hasValidationErrors() {
        return errors != null && errors.hasErrors();
    }

    public String[] getMessageArgs() {
        return messageArgs;
    }

    public void setMessageArgs(String[] messageArgs) {
        this.messageArgs = messageArgs;
    }
}
