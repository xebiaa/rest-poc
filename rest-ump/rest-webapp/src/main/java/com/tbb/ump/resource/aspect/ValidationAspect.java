package com.tbb.ump.resource.aspect;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.sun.jersey.api.client.ClientResponse;
import com.tbb.ump.resource.exception.ResourceBaseException;


/**
 * @author Jagmeet
 *
 */
@Aspect
public class ValidationAspect {
    @Autowired
    private BeanFactory beanFactory;

    @Before("execution(* com.tbb.ump.resource.*.*(..)) && @annotation(Validate)")
    public void validate(JoinPoint call) throws ResourceBaseException, SecurityException, NoSuchMethodException {
	System.out.println("Running validation");
	MethodSignature methodSignature = (MethodSignature) call.getSignature();
	methodSignature.getDeclaringType();
	Class<?> classObject = call.getTarget().getClass();
	Method method = classObject.getMethod(methodSignature.getName(),
		methodSignature.getParameterTypes());
	Validate validate = getValidateAnnotation(method);
	System.out.println("Validate retrieved: " + validate);
	if (validate != null) {
	    List<Object> arguments = findAllArguments(call);
	    ValidatorFactory factory =
		    Validation.buildDefaultValidatorFactory();
	    Validator validator = factory.getValidator();
	    Set<ConstraintViolation<Object>> constraintViolations = new HashSet<ConstraintViolation<Object>>();
	    for (Object argument : arguments) {
		constraintViolations.addAll(validator.validate(argument));
	    }
	    
	    /*if (!constraintViolations.isEmpty()) {
		throw new ResourceBaseException(ClientResponse.Status.BAD_REQUEST, constraintViolations.iterator().next().getMessage());
	    }*/
	    
	    List<Validator> springValidators = getValidators(validate);
	    for (Validator hibernateValidator : springValidators) {
		arguments = findArguments(hibernateValidator, call);
		for (Object argument : arguments) {
		    /*Errors errors = (Errors) call.getArgs()[1];
		    springValidator.validate(argument, errors);*/
		    constraintViolations.addAll(hibernateValidator.validate(argument));
		}
	    }
	    
	    if (!constraintViolations.isEmpty()) {
		throw new ResourceBaseException(ClientResponse.Status.BAD_REQUEST, constraintViolations.iterator().next().getMessage());
	    }
	}
    }

    private Validate getValidateAnnotation(AnnotatedElement annotatedMethod) {
	Validate validateAnnotation = null;
	Annotation[] classAnnotations = annotatedMethod.getAnnotations();
	for(Annotation annotation : classAnnotations)
	{
	    if (annotation instanceof Validate)
	    {
		validateAnnotation = (Validate) annotation;
	    }
	}

	return validateAnnotation;
    }

    /*
     * Read annotation to get validators if applied.
     */
    private List<Validator> getValidators(Validate validateAnnotation) {
	List<Validator> validators = new LinkedList<Validator>();
	if (validateAnnotation.validators() != null
		&& validateAnnotation.validators().length > 0) {
	    for (Class<?> validatorclazz : validateAnnotation.validators()) {
		Validator validator = (Validator) beanFactory.getBean(validatorclazz);
		if (validator != null) {
		    validators.add(validator);
		}
	    }

	}
	return validators;
    }

    /**
     * Called to process each service method argument passed.
     *
     * @param validator the candidate validator
     * @param call the join point being executed
     * @return the argument that needs to be validated
     */
    private List<Object> findArguments(Validator validator, JoinPoint call) {
	List<Object> arguments = new LinkedList<Object>();

	if (call.getArgs() != null 
		&& call.getArgs().length > 0) {
	    for (Object arg : call.getArgs()) {
		if (arg != null && arg.getClass().isAssignableFrom(validator.getClass())) {
		    arguments.add(arg);
		}
	    }
	}

	return arguments;
    }

    private List<Object> findAllArguments(JoinPoint call) {
	List<Object> arguments = new LinkedList<Object>();

	if (call.getArgs() != null 
		&& call.getArgs().length > 0) {
	    for (Object arg : call.getArgs()) {
		arguments.add(arg);
	    }
	}
	return arguments;
    }
}