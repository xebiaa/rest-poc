package com.tbb.ump.resource;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tbb.ump.common.utils.UMPDateUtil;
import com.tbb.ump.domain.WaterContainer;
import com.tbb.ump.resource.aspect.Validate;
import com.tbb.ump.resource.exception.ResourceBaseException;
import com.tbb.ump.service.WaterContainerService;

@Component
@Path("waterContainer")
public class WaterContainerRestService {

    @Autowired
    private WaterContainerService waterContainerService;
    
   

    @GET
    @Path("user/{userId}/userTimeZone/{userTimeZoneId}/date/{userDate}")
    @Produces(MediaType.APPLICATION_JSON)
    public WaterContainer getWaterContainer(
            @PathParam("userId") Long userId,
            @PathParam("userTimeZoneId") String userTimeZoneId,
            @PathParam("userDate") String userDate
    )  throws ResourceBaseException {
        try {
            WaterContainer waterContainer = waterContainerService.getWaterContainer(UMPDateUtil.getFormattedDate(userDate), userTimeZoneId, userId);
			return waterContainer;

        } catch (ParseException e) {
            throw new ResourceBaseException(e.getMessage());
        }
    }

    @GET
    @Path("user/{userId}/date/{date}/numOfDays/{numOfDays}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<WaterContainer> getWaterContainerForLastNDays(
            @PathParam("userId") Long userId,
            @PathParam("numOfDays") int numOfDays,
            @PathParam("date") String date) throws ResourceBaseException {
        try {
            List<WaterContainer> waterContainerForLastNDays = waterContainerService.getWaterContainerForLastNDays(userId, UMPDateUtil.getFormattedDate(date), numOfDays);
            return waterContainerForLastNDays;
        } catch (ParseException e) {
            throw new ResourceBaseException(e.getMessage());
        }
    }

    @Validate
    @POST
    @Path("save")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public WaterContainer saveWaterCups(WaterContainer  waterContainer) {
        return waterContainerService.saveWaterCups(waterContainer.getId(), waterContainer.getCupsOfWater());
    }

    @Validate
    @POST
    @Path("addCup")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public WaterContainer addWaterCup(WaterContainer  waterContainer) {
        return waterContainerService.addWaterCup(waterContainer.getId(),  waterContainer.getCupsOfWater());
    }

    @Validate
    @POST
    @Path("removeCup")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public WaterContainer subtractWaterCup(WaterContainer  waterContainer) {
        return waterContainerService.subtractWaterCup(waterContainer.getId(),  waterContainer.getCupsOfWater());
    }

    @POST
    @Path("save")
    @Produces(MediaType.APPLICATION_JSON)
    public WaterContainer addWaterContainer(@FormParam("userId") Long userId,
    		@FormParam("userTimeZoneId") String userTimeZoneId,
    		@FormParam("userDate") String userDate) throws ResourceBaseException {
        try {
            return waterContainerService.addWaterContainer(UMPDateUtil.getFormattedDate(userDate), userId, userTimeZoneId);
        } catch (ParseException e) {
            throw new ResourceBaseException(e.getMessage());
        }
    }


}
