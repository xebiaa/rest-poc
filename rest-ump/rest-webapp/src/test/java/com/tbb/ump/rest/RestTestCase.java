package com.tbb.ump.rest;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.tbb.ump.domain.Person;
import org.junit.Test;

import javax.ws.rs.core.MediaType;

/**
 * Created by IntelliJ IDEA.
 * User: vikas
 * Date: 8/10/12
 * Time: 7:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class RestTestCase {

    @Test
    public void testGetPerson() {
//	ClientConfig config = new DefaultClientConfig();
//        config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
//        Client client = Client.create(config);
//        WebResource webResource = client.resource("http://localhost:8080/rest-poc/webresources/sayhello/person");
//        Person person = webResource.accept(MediaType.APPLICATION_JSON_TYPE).get(Person.class);
//        Assert.assertEquals("Vikas", person.getFirstName());
//        Assert.assertEquals("Gupta", person.getLastName());
    } 

    @Test
    public void testExceptionPath() {
	/*ClientConfig config = new DefaultClientConfig();
        config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
        Client client = Client.create(config);
        WebResource webResource = client.resource("http://localhost:8080/rest-poc/webresources/sayhello/exceptionPath");
        Builder builder = webResource.accept(MediaType.APPLICATION_JSON_TYPE);
        Assert.assertEquals(ClientResponse.Status.BAD_REQUEST.getStatusCode(), builder.head().getStatus());*/
    }

    public static void main(String[] args) {
	    ClientConfig config = new DefaultClientConfig();
        config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
        Client client = Client.create(config);
        WebResource webResource = client.resource("http://localhost:8080/rest-poc/webresources/sayhello/person/add"); 
        Person person = webResource.accept(MediaType.APPLICATION_JSON_TYPE).get(Person.class);
        System.out.println(person.getFirstName());
    }
}
